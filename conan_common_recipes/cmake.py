from conans import ConanFile
from conans import CMake
from conans.tools import collect_libs
from conan_common_recipes.headeronly import package_headers
from conan_common_recipes.require_scm import RequireScm
import os

class CMakePackage(ConanFile, RequireScm):
    def _verbose_makefile(self):
        return os.environ.get('CONAN_' + self.name.upper() + '_VERBOSE_MAKEFILE') is not None

    def _cmake_defs_from_options(self):
        defs = {}

        for name, value in self.options.values.as_list():
            defs[(self.name.upper() + '_' + name.upper()).replace('-', '_')] = value

        defs['CMAKE_VERBOSE_MAKEFILE'] = True

        if 'shared' in self.options:
            defs['CMAKE_BUILD_SHARED_LIBS'] = self.options.shared

        return defs

    @property
    def _custom_cmake_defs(self):
        return getattr(self, 'custom_cmake_defs', {})

    def _parallel_build(self):
        return os.environ.get('CONAN_' + self.name.upper() + '_SINGLE_THREAD_BUILD') is None

    def build(self):
        cmake = CMake(self, parallel=self._parallel_build())
        cmake.configure(defs={**self._cmake_defs_from_options(), **self._custom_cmake_defs}, source_folder=self._repository_path)
        cmake.build()

    def package(self):
        package_headers(self)
        self.copy('*.a', dst='lib', keep_path=False)
        self.copy('*.so', dst='lib', keep_path=False)
        self.copy('*.lib', dst='lib', keep_path=False)
        self.copy('*.dll', dst='lib', keep_path=False)

    def package_info(self):
        self.output.info("Collecting package libs...")
        self.cpp_info.libs = collect_libs(self)

